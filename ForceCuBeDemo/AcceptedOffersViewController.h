//
//  FCBAcceptedOffersViewController.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 01/02/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ForceCuBeSDK.h"
#import "ForceCuBeUI.h"

@interface AcceptedOffersViewController : UITableViewController

@property ForceCuBe * forcecube;

@end
