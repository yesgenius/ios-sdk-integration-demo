//
//  ViewController.m
//  ForceCuBeDemo
//
//  Created by Stanislav Olekhnovich on 02/03/16.
//  Copyright © 2016 FORCECUBE. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "AcceptedOffersViewController.h"

#import "ForceCuBeSDK.h"

@interface ViewController () <FCBDelegate, FCBCampaignManagerDelegate>

@property ForceCuBe * forcecube;
@property CLLocationManager * locationManager;

@end

@implementation ViewController

//**************************************************************************************************
- (void) viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.forcecube = appDelegate.forcecube;
    
    self.forcecube.delegate = self;
    self.forcecube.campaignManager.delegate = self;
    
    self.locationManager = [CLLocationManager new];
    
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways)
    {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.forcecube start];
    });
}

//**************************************************************************************************
- (void) manager: (id<FCBCampaignManager>) campaignManager
didDeliverCampaignOffer: (id<FCBCampaignOffer>) campaignOffer
{
    UILocalNotification * ln = [UILocalNotification new];
    
    ln.alertBody = [campaignOffer.notificationText stringByReplacingOccurrencesOfString: @"%" withString: @"%%" options: 0  range: NSMakeRange(0, campaignOffer.notificationText.length)];
    ln.soundName = UILocalNotificationDefaultSoundName;
    ln.userInfo = @{ kNotificationCampaignIdKey: @(campaignOffer.campaignOfferId)};
    ln.applicationIconBadgeNumber = campaignManager.unopenedOffers.count;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow: ln];
}

//**************************************************************************************************
- (void) manager: (id<FCBCampaignManager>)campaignManager hasAcceptedOffers: (NSArray<id<FCBCampaignOffer>> *)campaignOffers forCurrentLocation:(id<FCBLocation>)location advertisers: (NSArray<id<FCBAdvertiser>> *) advertisers
{
    UILocalNotification * ln = [UILocalNotification new];
    
    NSString * names = [[advertisers valueForKey: @"name"] componentsJoinedByString: @","];
    
    ln.alertBody = [NSString stringWithFormat: @"You have saved offers in %@ from %@", location.name, names];
    ln.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow: ln];
}

//**************************************************************************************************
- (void) forceCuBe: (ForceCuBe *) fcb didRunIntoError: (NSError *) error
{
    [self showAlertWithTitle: @"Error"
                     message: [NSString stringWithFormat: @"%@", error]];
}

//**************************************************************************************************
- (void) showAlertWithTitle: (NSString *) title message: (NSString *) message
{
    UIAlertController * ac = [UIAlertController alertControllerWithTitle: title
                                                                 message: message
                                                          preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction * quitAction = [UIAlertAction actionWithTitle: @"OK"
                                                          style: UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                        }];
    [ac addAction: quitAction];
    
    [self presentViewController: ac animated: YES completion: nil];
}

//**************************************************************************************************
- (void) forceCuBe: (ForceCuBe *) fcb didChangeStatus: (FCBStatus) status error: (NSError *) error
{
    if (status == FCBStatusStopped)
    {
        if (error.code == kFCBUserErrorBackgroundLocationTrackingIsOff)
        {
            [self showAlertWithTitle: @"Error" message: @"Access to user location in background is denied."];
        }
        else if (error.code == kFCBNoNetworkOnInitError)
        {
            [self showAlertWithTitle: @"Error" message: @"No network access while initializing"];
        }
        else if (error.domain == kFCBServerErrorDomain)
        {
            [self showAlertWithTitle: @"Error" message: @"Server error while initializing"];
        }
        else if (error.code == kFCBUnrecoverableErrorAuthFailure)
        {
            [self showAlertWithTitle: @"Error" message: @"Wrong dev key or/and dev secret"];
        }
        else if (error.domain == kFCBRegionMonitoringErrorDomain)
        {
            [self showAlertWithTitle: @"Error" message: @"Failed to start location manager, please restart."];
        }
        else if (error.code == kFCBUnrecoverableErrorBLEIsUnsupported)
        {
            [self showAlertWithTitle: @"Error" message: @"BLE is not supported on this device"];
        }
        else if (error.code == kFCBUnrecoverableErrorBLEAccessIsUnauthorized)
        {
           [self showAlertWithTitle: @"Error" message: @"App doesnt have permission to access BLE"];
        }
    }
    else if (status == FCBStatusStartedWithGeofencing)
    {
        [self showAlertWithTitle: @"Warning" message: @"Bluetooth is off."];
    }
}

//**************************************************************************************************
- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    if ([[segue identifier] isEqualToString:@"TO_OFFERS_LIST"])
    {
        AcceptedOffersViewController * vc = [segue destinationViewController];
        
        vc.forcecube = self.forcecube;
    }
}


@end
