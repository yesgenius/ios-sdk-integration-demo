//
//  AppDelegate.m
//  ForceCuBeDemo
//
//  Created by Stanislav Olekhnovich on 02/03/16.
//  Copyright © 2016 FORCECUBE. All rights reserved.
//

#import "AppDelegate.h"

#import "ForceCuBeSDK.h"
#import "ForceCuBeUI.h"


NSString * const kNotificationCampaignIdKey = @"kNotificationCampaignIdKey";

@interface AppDelegate () <FCBCampaignOfferViewControllerDelegate>
@end

@implementation AppDelegate

//**************************************************************************************************
- (BOOL) application: (UIApplication *) application didFinishLaunchingWithOptions: (NSDictionary *) launchOptions
{
    // Override point for customization after application launch.
    
    self.forcecube = [[ForceCuBe alloc] initWithAppDeveloperKey: @"825aa8cb-2020-485f-9ff7-6384e35787a5"
                                             appDeveloperSecret: @"test"
                                                     externalId: @"1234567890"];
    
    return YES;
}

//**************************************************************************************************
-(void) application: (UIApplication *) application didReceiveLocalNotification: (UILocalNotification *) notification
{
    if (notification.userInfo[kNotificationCampaignIdKey])
    {
        NSUInteger campaignId = [notification.userInfo[kNotificationCampaignIdKey] integerValue];
        
        FCBCampaignOfferViewController * offerVC = [self.forcecube campaignOfferControllerWithCampaignId: campaignId];
        
        offerVC.delegate = self;
        
        [(UINavigationController *) self.window.rootViewController pushViewController: offerVC animated: YES];
    }    
}

//**************************************************************************************************
- (void) offerViewControllerDidComplete: (FCBCampaignOfferViewController *) controller
                      withOfferAccepted: (BOOL) wasOfferAccepted
{
    [(UINavigationController *)self.window.rootViewController popViewControllerAnimated: YES];
}



@end
