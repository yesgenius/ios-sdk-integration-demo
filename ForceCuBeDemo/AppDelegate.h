//
//  AppDelegate.h
//  ForceCuBeDemo
//
//  Created by Stanislav Olekhnovich on 02/03/16.
//  Copyright © 2016 FORCECUBE. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kNotificationCampaignIdKey;

@class ForceCuBe;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property ForceCuBe * forcecube;

@end

